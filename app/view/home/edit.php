<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <h4 class="mt-3">Edit User</h4>
    <div class="row">
            <div class="col-lg-7">
                <div class="card">
                    <div class="card-group">
                        <div class="card-body">
                            <form method="POST" action="<?=BASE_URL?>/home/ubah">
                            <input type="hidden" name="id" value="<?= $data['data']['id']?>">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" required placeholder="username" name="username" value="<?= isset($data['data']) ? $data['data']['username'] : ''?>" >
                                </div>
                                <div class="form-group">
                                    <label class="mt-2">Email</label>
                                    <input type="text" class="form-control" required placeholder="email" name="email" value="<?= isset($data['data']) ? $data['data']['email'] : ''?>" >
                                </div>
                                <div class="form-group">
                                    <label class="mt-2">First Name</label>
                                    <input type="text" class="form-control" required placeholder="first name" name="first_name" value="<?= isset($data['data']) ? $data['data']['first_name'] : ''?>" >
                                </div>
                                <div class="form-group">
                                    <label class="mt-2">Last Name</label>
                                    <input type="text" class="form-control" required placeholder="last name" name="last_name" value="<?= isset($data['data']) ? $data['data']['last_name'] : ''?>" >
                                </div>
                                <a href="<?=BASE_URL?>/home/index" class="btn btn-dark mt-3"><i class="fas fa-arrow-left"></i>Back</a>
                                <button type="submit" class="btn btn-success mt-3"><i class="bi bi-save"></i>Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>