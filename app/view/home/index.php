<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <!-- <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div> -->
          <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar" class="align-text-bottom"></span>
            This week
          </button>
        </div>
      </div>
      <a href="<?=BASE_URL?>/home/createUser"><button type="button" class="btn btn-success mb-3">Tambah Data</button></a>
      <table class="table table-striped">
          <thead class="table-dark">
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nama</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Email</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>

          <?php
              $no = 1;
              foreach( $data['mhs'] as $data ) :
              ?>
            <tr>
              <th scope="row"><?= $no++ ?></th>
              <td><?= $data['username'] ?></td>
              <td><?= $data['first_name'] ?></td>
              <td><?= $data['last_name'] ?></td>
              <td><?= $data['email'] ?></td>
              <td>
                <a href="<?=BASE_URL?>/home/editUser/<?=  $data['id'] ?>" class="btn btn-danger"><i class="bi bi-pencil-square"></i></a>
                <a href="<?=BASE_URL?>/home/hapus/<?=  $data['id'] ?>" class="btn btn-warning"><i class="bi bi-trash3"></i></a>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
              <!-- <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas> -->
            </main>
          </div>
        </div>
      </table>
</main>