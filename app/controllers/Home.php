<?php 

class Home extends Controller{

    public function __construct()
    {
        if(!isset($_SESSION["login"])) {
            return header('Location: ' . BASE_URL . '/auth/login');
        }
    }
    public function index($company = 'SMKN1')
    {   
        $data['title'] = 'Home';
        $data['name'] = 'Pradnyana';
        $data['company'] = $company;
        $data['mhs'] = $this->model('User_model')->getAllUser();
        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer');
    }

    public function about($company = 'SMKN1')
    {
        $data['title'] = 'About';
        $data['name'] = 'Pradnyana';
        $data['company'] = $company;
        $this->view('templates/header', $data);
        $this->view('about/index', $data);
        $this->view('templates/footer');
    }

    public function createUser($company = 'SMKN1') {
        $data['title'] = 'Tambah Data User';
        $data['name'] = 'Pradnyana';
        $data['company'] = $company;
        $this->view('templates/header', $data);
        $this->view('home/create');
        $this->view('templates/footer');
    }
    public function editUser($id) {
        $data['title'] = 'Edit Data User';
        $data['data'] = $this->model('User_model')->getUserById($id);
        $this->view('templates/header', $data);
        $this->view('home/edit', $data);
        $this->view('templates/header');
    }

    public function create() {
        if ($this->model('User_model')->create($_POST) > 0) {
            Flasher::setFlash('berhasil', 'ditambahkan', 'success');
            header('Location: '.BASE_URL. '/home');
            exit;
        }
    }

    public function ubah()
    {
        if ($this->model('User_model')->edit($_POST) > 0) {
            Flasher::setFlash('berhasil', 'diedit', 'success');
            header('Location: '.BASE_URL. '/home');
            exit;
        }
    }

    public function hapus($id)
    {
        $result = $this->model('User_model')->hapus($id);
        if($result > 0) {
            Flasher::setFlash('', 'Data Berhasil Di Hapus', 'success');
            return header("Location: " . BASE_URL . "/home");
            exit;
        } else {
            Flasher::setFlash('', 'Gagal Hapus Data', 'danger');
            return header("Location: " . BASE_URL . "/home");
            exit;
        }
    }
}